from django.shortcuts import render
from django.conf import settings
# Create your views here.
import json
import os
from django.shortcuts import render

# from ane.dependencies
managementjson = os.path.join(settings.MEDIA_ROOT,"management.json")

def management(request):
    try:
        with open(managementjson) as f:
            data = json.load(f)
    except:
        data={}
    jsondata = json.dumps(data)
    context={"jsonData":jsondata}
    context.update(data)
    return render(request,"management/management.html",context)