# Generated by Django 4.2.5 on 2023-09-12 04:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='inventory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=500, null=True)),
                ('iin', models.CharField(blank=True, default=None, max_length=200, null=True)),
                ('cost', models.DecimalField(blank=True, decimal_places=4, default=0, max_digits=20, null=True)),
                ('quantity', models.IntegerField(blank=True, default=0, null=True)),
                ('quantity_sold', models.IntegerField(blank=True, default=0, null=True)),
                ('selling_price', models.DecimalField(blank=True, decimal_places=4, default=0, max_digits=20, null=True)),
                ('discount_amount', models.DecimalField(blank=True, decimal_places=4, default=0, max_digits=20, null=True)),
                ('profit_earned', models.DecimalField(blank=True, decimal_places=4, default=0, max_digits=20, null=True)),
                ('revenue', models.DecimalField(blank=True, decimal_places=4, default=0, max_digits=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='transaction',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=500, null=True)),
                ('transaction_quantity', models.IntegerField(blank=True, default=0, null=True)),
                ('selling_price', models.DecimalField(blank=True, decimal_places=4, default=0, max_digits=20, null=True)),
                ('transactiondatetime', models.DateTimeField(auto_now_add=True)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='IMS.inventory')),
            ],
        ),
        migrations.CreateModel(
            name='order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=500, null=True)),
                ('order_quantity', models.IntegerField(blank=True, default=0, null=True)),
                ('order_cost', models.DecimalField(blank=True, decimal_places=4, default=0, max_digits=20, null=True)),
                ('order_datetime', models.DateTimeField(auto_now_add=True)),
                ('is_received', models.BooleanField(default=False)),
                ('is_cancelled', models.BooleanField(default=False)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='IMS.inventory')),
            ],
        ),
    ]
