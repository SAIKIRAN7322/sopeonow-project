from django.shortcuts import render,redirect
from .forms import inventoryform,orderform,transactionform,viewinventoryform
from .models import inventory,order,transaction
from django.http import HttpResponse
from django.contrib import messages
from django.db.models import Sum,Max,Count
import decimal

def home(request):
    store_profit=inventory.objects.aggregate(total_profit=Sum("profit_earned"))["total_profit"]
    total_items_in_stock=inventory.objects.aggregate(total_quantity=Sum("quantity"))["total_quantity"]
    item_with_highest_cost=inventory.objects.annotate(max_cost=Max('cost')).order_by("-max_cost").first()
    item_with_highest_profits=inventory.objects.annotate(max_profit=Max('profit_earned')).order_by("-max_profit").first()
    item_most_sold = inventory.objects.annotate(transaction_count=Count('transaction')).order_by('-transaction_count').first()
    item_out_of_stock=list(inventory.objects.filter(quantity=0).values_list("name"))
    items_out_of_stock=[]
    for i in item_out_of_stock:
        items_out_of_stock.append(i[0])

    dict={"storeprofit":store_profit,"total_items_in_stock":total_items_in_stock,"item_with_highest_cost": item_with_highest_cost,"item_with_highest_profits":item_with_highest_profits,
          'item_most_sold':item_most_sold,"items_out_of_stock":items_out_of_stock}
    
    return render(request,"dashboard.html",dict)


def createiteminventory(request):
    dict={"createitemform":inventoryform}
    if request.method=="POST":
        formdata=inventoryform(request.POST)
        if formdata.is_valid():
            formdata.save()
        return redirect("/home")
    return render(request,"createitem.html",dict)


def itemslist(request):
    items=inventory.objects.all()
    k={"items":items}
    return render(request,"home.html",k)



def updateitem(request,id):
    item=inventory.objects.get(id=id)
    form=inventoryform(instance=item)
    if request.method=="POST":
        formdata=inventoryform(request.POST)
        if formdata.is_valid():
            formdata.save()
            return redirect("/home")
    dict={"form":form}
    return render(request,"updateitem.html",dict)

def viewitem(request,id):
    item=inventory.objects.get(id=id)
    print(item)
    dict={"form":item}
    return render(request,"itemdetail.html",dict)


def deleteiteminventory(request,id):
    item=inventory.objects.get(id=id)
    item.is_deleted=True
    item.save()
    return redirect("/home")

def orderlist(request,id):
    if id:
        try:
            orders=order.objects.filter(item__id=id)
            count=orders.count()
        except Exception as e:
            print(e)
            return HttpResponse("There are no orders available for the entered item")
    else:
        orders=order.objects.all()
        count=orders.count()
    k={"items":orders,"count":count}
    return render(request,"orderlist.html",k)


def createorder(request,id):
    inventory_obj=inventory.objects.get(id=id)
    form=orderform(initial={'item': id,'name':f"{inventory_obj.name}_order"})
    dict={"createorderform":form}
    if request.method=="POST":
        formdata=orderform(request.POST)
        if formdata.is_valid():
            formdata.save()
        return redirect("/orderslist/0")
    return render(request,"createorder.html",dict)

def updateorder(request,id,is_received,is_cancelled):
    orderobj=order.objects.get(id=id)
    if is_received:
        orderobj.is_received=is_received
        item_obj=orderobj.item
        item_obj.quantity+=orderobj.order_quantity
        orderobj.order_cost=orderobj.item.quantity*orderobj.item.cost
        item_obj.save()
    if is_cancelled:
        orderobj.is_cancelled=is_cancelled
    orderobj.save()
    return redirect(f"/orderslist/{id}")
def particularorders(request,id,is_received,is_cancelled):
    if  is_received=='True':
        if id:
            orderobjects=order.objects.filter(item__id=id,is_received=True)
            if not orderobjects.exists():
                return HttpResponse("There are no orders received available for the entered item")
            else:
                k={"items":orderobjects}
                return render(request,"received_orders.html",k)
        else:
            orderobjects=order.objects.filter(is_received=True)
            k={"items":orderobjects}
            return render(request,"received_orders.html",k)


    elif is_cancelled=='True':
        if id:
            orderobjects=order.objects.filter(item__id=id,is_cancelled=True)
            if not orderobjects.exists():
                return HttpResponse("There are no orders cancelled available for the entered item")
            else:
                k={"items":orderobjects}
                return render(request,"cancelled_orders.html",k)
        else:
            orderobjects=order.objects.filter(is_cancelled=True)
            k={"items":orderobjects}
            return render(request,"cancelled_orders.html",k)
    return render(request,"orderlist.html",k)


def createtransaction(request,id):
    inventory_obj=inventory.objects.get(id=id)
    form=transactionform(initial={'item': id,'name':f"{inventory_obj.name}_transaction"})
    dict={"createtransactionform":form}
    if request.method=="POST":
        formdata=transactionform(request.POST)
        if formdata.is_valid():
            if float(inventory_obj.quantity)<float(formdata.data["transaction_quantity"]):
                messages.info(request,"The Entered transaction quantity is greater than item stock quantity")
            else:
                inventory_obj=inventory.objects.get(id=formdata.data["item"])
                inventory_obj.quantity-=int(formdata.data['transaction_quantity'])
                inventory_obj.quantity_sold+=int(formdata.data['transaction_quantity'])
                inventory_obj.selling_price=float(inventory_obj.selling_price*int(formdata.data['transaction_quantity']))
                inventory_obj.profit_earned+=decimal.Decimal(float(formdata.data['transaction_quantity'])*((float(inventory_obj.cost)) - float(inventory_obj.selling_price)))
                inventory_obj.save()
                inventory_obj.revenue=float(inventory_obj.quantity_sold*inventory_obj.cost)
                inventory_obj.save()
                formdata.save()
                return redirect("/transactionslist/0")
    return render(request,"createtransaction.html",dict)

def transactionslist(request,id):
    if id:
        try:
            transactions=transaction.objects.filter(item__id=id)
            count=transactions.count()
        except:
            return HttpResponse("There are no transactions available for the entered item")
    else:
        transactions=transaction.objects.all()
        count=transactions.count()
    k={"items":transactions,"count":count}
    return render(request,"transactionslist.html",k)
