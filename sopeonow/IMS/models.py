from django.db import models
import random
import string
import datetime
# Create your models here.
class inventory(models.Model):
    name=models.CharField(max_length=500,default=None, blank=True, null=True)
    iin=models.CharField(max_length=200,default=None, blank=True, null=True )
    cost=models.DecimalField(max_digits=20, decimal_places=4,default=0, blank=True, null=True)
    quantity=models.IntegerField(default=0,blank=True, null=True)
    quantity_sold=models.IntegerField(default=0,blank=True, null=True)
    selling_price=models.DecimalField(max_digits=20, decimal_places=4,default=0, blank=True, null=True)
    profit_earned=models.DecimalField(max_digits=20, decimal_places=4,default=0, blank=True, null=True)
    revenue=models.DecimalField(max_digits=20, decimal_places=4,default=0, blank=True, null=True)
    is_deleted=models.BooleanField(default=False)
    
    def save(self, *args, **kwargs):
        if not self.iin:
            current_date = datetime.datetime.now().strftime("%d")
            current_year = datetime.datetime.now().strftime("%y")
            current_month=datetime.datetime.now().strftime("%m")
            if len(self.name) > 2:
                item_name = self.name[:2]
            elif len(self.name) < 2:
                item_name = self.name.ljust(2, '0')
            iin = item_name.upper() + current_date + current_year+current_month

            self.iin=iin
            while  inventory.objects.filter(iin=iin).exists():
                if len(self.name) > 2:
                    item_name = self.name[:2]
                elif len(self.name) < 2:
                    item_name = self.name.ljust(2, '0')
                    iin = item_name.upper() + current_date + current_year+current_month
            self.iin=iin
        super(inventory, self).save(*args, **kwargs)
    def __str__(self):
        return f"{self.name}-{self.iin}"
    class Meta:
        verbose_name = "Inventory"
        verbose_name_plural = "Inventory"


class order(models.Model):
    name=models.CharField(max_length=500,default=None, blank=True, null=True)
    item=models.ForeignKey(inventory,on_delete=models.CASCADE)
    order_quantity=models.IntegerField(default=0)
    order_cost=models.DecimalField(max_digits=20, decimal_places=4,default=0, blank=True, null=True)
    order_datetime=models.DateTimeField(auto_now_add=True)
    is_received=models.BooleanField(default=False)
    is_cancelled=models.BooleanField(default=False)
    def __str__(self):
        return f"{self.name}"
    class Meta:
        verbose_name = "Order"
        verbose_name_plural = "Orders"

class transaction(models.Model):
    name=models.CharField(max_length=500,default=None, blank=True, null=True)
    item=models.ForeignKey(inventory,on_delete=models.CASCADE)
    transaction_quantity=models.IntegerField(default=0,blank=True, null=True)
    selling_price=models.DecimalField(max_digits=20, decimal_places=4,default=0, blank=True, null=True)
    transactiondatetime=models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f"{self.name}" 
    class Meta:
        verbose_name = "Transaction"
        verbose_name_plural = "Transactions"