from django import forms
from .models import *
from django.contrib import messages
class inventoryform(forms.ModelForm):
    class Meta:
        model = inventory
        fields = ["name","cost","selling_price"]

class viewinventoryform(forms.ModelForm):
    class Meta:
        model=inventory
        fields="__all__"

class orderform(forms.ModelForm):
    class Meta:
        model = order
        exclude=('order_cost',"is_received","is_cancelled")

class transactionform(forms.ModelForm):
    class Meta:
        model=transaction
        fields=["transaction_quantity","item","name"]


