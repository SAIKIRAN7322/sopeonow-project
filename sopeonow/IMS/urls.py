from django.contrib import admin
from django.urls import path,include
from .views import *
urlpatterns = [
    path("create-item/",createiteminventory),
    path("update-item/<int:id>",updateitem),
    path("delete-item/<int:id>",deleteiteminventory),
    path("view-item/<int:id>",viewitem),
    path("particularorderslist/<int:id>/<str:is_received>/<str:is_cancelled>/",particularorders),
    path("create-order/<int:id>",createorder),
    path("orderslist/<int:id>",orderlist),
    path("update-order/<int:id>/<str:is_received>/<str:is_cancelled>/",updateorder),
    path("itemslist/",itemslist),
    path("create-transaction/<int:id>",createtransaction),
    path("transactionslist/<int:id>",transactionslist),
    path("home/",home)
]
