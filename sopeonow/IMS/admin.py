from django.contrib import admin
from .models import transaction,inventory,order
# Register your models here.
admin.site.register(transaction)
admin.site.register(inventory)
admin.site.register(order)